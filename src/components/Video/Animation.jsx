import React from 'react'
import { useLocation, useRouteMatch } from 'react-router'
import { Link } from 'react-router-dom'
import queryString from 'query-string'
import { Button, ButtonGroup } from 'react-bootstrap'
export default function Animation() {
    let { url } = useRouteMatch();
    const location = useLocation()
    // const query=new URLSearchParams(location.search)
    const query = queryString.parse(location.search)
    return (
        <div>
            <h3>Animation Category</h3>
            <ButtonGroup aria-label="Basic example">
                <Link to={`${url}?type=Action`}><Button variant="secondary">Action</Button></Link>
                <Link to={`${url}?type=Romance`}><Button variant="secondary">Romance</Button></Link>
                <Link to={`${url}?type=Comedy`}><Button variant="secondary">Comedy</Button></Link>
            </ButtonGroup>

            <h3>Please Choose Catogory : <span style={{ color: "red" }}>{query.type}</span></h3>

        </div>
    )
}
