import React from 'react'
import { Link, useRouteMatch, Switch, Route } from 'react-router-dom'
import Animation from './Animation';
import Movie from './Movie';
import { ButtonGroup, Button } from 'react-bootstrap'
export default function Video() {
    let { path, url } = useRouteMatch();
    return (
        <div className="container">
            <h2 >Video</h2>
            <ButtonGroup aria-label="Basic example">
                <Link to={`${url}/movie`}><Button variant="secondary">Movie</Button></Link>
                <Link to={`${url}/animation`}><Button variant="secondary">Animation</Button></Link>
            </ButtonGroup>
            <Switch>
                <Route path={`${url}/movie`} component={Movie} />
                <Route path={`${url}/animation`} component={Animation} />
            </Switch>
        </div>
    )
}
