import React from 'react'
import { useLocation, useRouteMatch } from 'react-router'
import { Link } from 'react-router-dom'
import queryString from 'query-string'
import { Button, ButtonGroup } from 'react-bootstrap'
export default function Movie() {
    let { url } = useRouteMatch();
    const location = useLocation()
    // const query=new URLSearchParams(location.search)
    const query = queryString.parse(location.search)
    console.log(query);
    return (
        <div>
            <h3>Movie Category</h3>
            <ButtonGroup aria-label="Basic example">
                <Link to={`${url}?type=Advanture`}><Button variant="secondary">Advanture</Button></Link>
                <Link to={`${url}?type=Crime`}><Button variant="secondary">Crime</Button></Link>
                <Link to={`${url}?type=Action`}><Button variant="secondary">Action</Button></Link>
                <Link to={`${url}?type=Romance`}><Button variant="secondary">Romance</Button></Link>
                <Link to={`${url}?type=Comedy`}><Button variant="secondary">Comedy</Button></Link>
            </ButtonGroup>
            <h3>Please Choose Catogory : <span style={{ color: "red" }}>{query.type}</span></h3>
        </div>

    )
}
