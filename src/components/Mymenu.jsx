import React from 'react'
import {Link} from 'react-router-dom'
import {Navbar,Nav,Container} from 'react-bootstrap'
export default function Mymenu() {
    return (
        <div>
        <Navbar bg="light" expand="lg">
<Container>
  <Navbar.Brand href="/">Router</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="me-auto">
      <Nav.Link as={Link} to="/">Home</Nav.Link>
      <Nav.Link as={Link} to="/video">Video</Nav.Link>
      <Nav.Link as={Link} to="/account">Account</Nav.Link>
      <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
      <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
     
    </Nav>
  </Navbar.Collapse>
</Container>
</Navbar>  
      </div>
    )
}
