import React from 'react'
import {Button} from 'react-bootstrap'
export default function Welcom({onSignin}) {
    return (
        <div className="container">
            <h2>Welcome</h2>
            <Button onClick={onSignin} variant="primary" type="button" style={{margin:"0 auto"}}>
    Logout
  </Button>
        </div>
    )
}
