import React from 'react'
import { Redirect } from 'react-router'

export default function ProtectRouter({isSignin, component: Component ,path}) {
    if(isSignin){
        return  <Component to={path} />
    }else{
        return <Redirect to='/auth' />
}
}
