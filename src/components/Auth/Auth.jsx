import React from 'react'
import {Form,Button} from 'react-bootstrap'
import { useHistory } from 'react-router';
export default function Auth({setSignin}) {
      let history = useHistory();
      
      const redirect = () => {
        history.push('/welcome')
        setSignin(true)
      }
    return (
        <div>
            <h2 className="container" style={{textAlign:"center"}}>Login</h2>
            <Form className="w-50" style={{margin:"0 auto"}}>
  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" />
    <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>
  <Form.Group className="mb-3" controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Check me out" />
  </Form.Group>
  <Button onClick={redirect} variant="primary" type="button">
    Login
  </Button>
</Form>
        </div>
    )
    
}
