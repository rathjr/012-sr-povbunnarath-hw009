import React from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
export default function testview() {
    return (
        <div className="container">
            <Row>
             <Col xs="3" style={{ display: "flex", justifyContent: "space-between", marginTop: "10px" }}>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src="https://mcleansmartialarts.com/wp-content/uploads/2017/04/default-image-620x600.jpg" style={{ width: "100%", height: "180px" }} />
                            <Card.Body>
                                <Card.Title>title</Card.Title>
                                <Card.Text>
                                  description
                                </Card.Text>
                            
                              <Button variant="primary">Read </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    </Row>
        </div>
    )
}
