import React from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'
export default function Home({ list }) {
    console.log("list", list);
    return (

        <div className="container">
            <Row>
                {list.map((item, index) => (
                    <Col xs="3" style={{ display: "flex",justifyContent: "space-between", marginTop: "10px" }}>
                        <Card style={{ width: '18rem' }}>
                            <Card.Img variant="top" src={item.image} style={{ width: "100%", height: "180px" }} />
                            <Card.Body>
                                <Card.Title>{item.title}</Card.Title>
                                <Card.Text>
                                    {item.des}
                                </Card.Text>
                                <Link to={'/detail/'+item.id}><Button variant="primary">Read </Button></Link>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}

            </Row>
        </div>

    )
}
