import React from 'react'
import { useParams } from 'react-router'

export default function Detail() {
    let param = useParams()
    return (
        <div className="container">
            <h2>Details : {param.id}</h2>
        </div>
    )
}
