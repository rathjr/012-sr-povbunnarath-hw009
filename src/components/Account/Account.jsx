import React from 'react'
import { Route, Switch, Link, useRouteMatch } from 'react-router-dom'
import ReadAccountID from './ReadAccountID';
import { useParams } from 'react-router'
export default function Account() {
    let { path, url } = useRouteMatch();
    let param = useParams()
    // console.log("url",url);
    // console.log("path",path);
    return (
        <div className="container">
            <h2>Account</h2>
            <ul>
                <li>
                    <Link to={`${url}/netflix`}>Netflix</Link>
                </li>
                <li>
                    <Link to={`${url}/zillo-group`}>Zillo_Group</Link>
                </li>
                <li>
                    <Link to={`${url}/yahho`}>Yahho</Link>
                </li>
                <li>
                    <Link to={`${url}/modus-create`}>Modus_Create</Link>
                </li>
            </ul>
            <Switch>
                <Route path={`${path}/:id`} component={ReadAccountID} />
            </Switch>
            <h3>ID : {param.id} </h3>
        </div>
    )
}
